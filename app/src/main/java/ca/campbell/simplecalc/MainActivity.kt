package ca.campbell.simplecalc

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.util.Log
import android.widget.Toast

//  TODO: add a field to input a 2nd number, get the input and use it in calculations
//  TODO: the inputType attribute forces a number keyboard, don't use it on the second field so you can see the difference

//  TODO: add buttons & methods for subtract, multiply, divide

//  TODO: add input validation: no divide by zero
//  TODO: input validation: set text to show error when it occurs

//  TODO: add a clear button that will clear the result & input fields

//  TODO: the hint for the result widget is hard coded, put it in the strings file

class MainActivity : Activity() {
    // Get a handle on the views in the UI
    // lateinit allows us to avoid using null initialization & !! & ?
    // but we must be sure to init before use otherwise
    // it acts as if !! was used & crashes on null value
    private lateinit var etNumber1: EditText
    private lateinit var etNumber2: EditText
    private lateinit var result: TextView
    private val TAG = "CALC"
    internal var num1: Double = 0.toDouble()
    internal var num2: Double = 0.toDouble()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        etNumber1 = findViewById<EditText>(R.id.num1)
        etNumber2 = findViewById<EditText>(R.id.num2)
        result = findViewById<TextView>(R.id.result)
        }  //onCreate()

    // TODO: replace with code that adds the two input numbers
    fun addNums(v: View) {
        num1 = java.lang.Double.parseDouble(etNumber1.text.toString())
        num2 = java.lang.Double.parseDouble(etNumber2.text.toString())
        result.text = (num1 + num2).toString()
        Log.i(TAG, "User added " + num1 + " and " + num2)
    }

    fun subNums(v: View) {
        num1 = java.lang.Double.parseDouble(etNumber1.text.toString())
        num2 = java.lang.Double.parseDouble(etNumber2.text.toString())
        result.text = (num1 - num2).toString()
        Log.i(TAG, "User subtracted " + num2 + " from " + num1)
    }

    fun divNums(v: View) {
        num1 = java.lang.Double.parseDouble(etNumber1.text.toString())
        num2 = java.lang.Double.parseDouble(etNumber2.text.toString())
        if(num2.equals(0.0)) {
            result.text = getString(R.string.zerostr)
            Log.i(TAG, "User attempted to divide by zero")
            Toast.makeText(applicationContext,"You cannot divide by zero!",Toast.LENGTH_SHORT).show()
        } else {
            result.text = (num1 / num2).toString()
            Log.i(TAG, "User divided " + num1 + " by " + num2)
        }
    }

    fun mulNums(v: View) {
        num1 = java.lang.Double.parseDouble(etNumber1.text.toString())
        num2 = java.lang.Double.parseDouble(etNumber2.text.toString())
        result.text = (num1 * num2).toString()
        Log.i(TAG, "User multiplied " + num1 + " and " + num2)
    }

    fun clrall(v: View) {
        result.text = getString(R.string.answer_hint)
        etNumber1.text.clear()
        etNumber2.text.clear()
        Log.i(TAG, "User cleared the fields")
    }

}